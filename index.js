#!/usr/bin/env node
const request = require('request')
const fs = require('fs')
const shell = require('shelljs')
const clipboardy = require('clipboardy')
const opn = require('opn')

function upload (filePath) {
  var file = fs.createReadStream(filePath)

  var options = {
    uri: 'http://shekels.wtf/api/upload',
    port: 80,
    method: 'POST',
    formData: {
      key: 'akey', 
      image: file
    },
    json: true
  }

  request.post(options, function (error, response, body) {
    console.log('Request')
    if (!error && response.statusCode === 200) {
      console.log('Succeeded!')
      clipboardy.writeSync(body.body)
      shell.exec(`firefox ${body.body}`)
      console.log(body.body)
    } else {
      console.log('Failed :(')
      console.log(error)
      console.log(response)
      console.log(body)
    }
  })
}

function screenshot () {
  console.log('Screenshotting')
  shell.exec('tmp=$(mktemp /tmp/XXXXXXXXXXXXXXXXXXX.png); maim -s $tmp; echo $tmp',
    function (code, stdout, stderr) {
      if (code === 0) {
        console.log('Uploading')
        upload(stdout.trim())
        shell.rm(stdout.trim())
      }
      console.log('Code:', code)
      console.log('Out:', stdout)
      console.log('Err:', stderr)
    }
  )
}

if (process.argv[2] != null) {
  upload(process.argv[2])
} else {
  screenshot()
}
